<?php
namespace Modules\Whatshub\Actions;

use Modules\Whatshub\Models\Outbox;

class MarkInvalidNumberOutbox
{
    public static function run(Outbox $outbox)
    {
        return $outbox->update(['status' => -1]);
    }
}
