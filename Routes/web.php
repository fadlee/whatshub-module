<?php

use Illuminate\Support\Facades\Route;
use Modules\Whatshub\Http\Livewire\ContactPage;
use Modules\Whatshub\Http\Livewire\ContactListPage;
use Modules\Whatshub\Http\Livewire\WhatshubPage;
use Modules\Whatshub\Http\Livewire\OutboxPage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth:sanctum', 'verified', 'module:whatshub'])
    ->prefix('admin/whatshub')
    ->as('admin.whatshub.')
    ->group(function () {
        Route::get('/', WhatshubPage::class)
            ->name('index');
        Route::get('/outbox', OutboxPage::class)
            ->name('outbox');
        Route::get('/list', ContactListPage::class)
            ->name('list');
        Route::get('/broadcast', ContactPage::class)
            ->name('broadcast');
    });
