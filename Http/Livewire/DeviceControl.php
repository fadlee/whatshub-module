<?php

namespace Modules\Whatshub\Http\Livewire;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Livewire\Component;

class DeviceControl extends Component
{
    public $workerUrl;
    public $devID;

    public function __construct()
    {
        parent::__construct();

        $this->workerUrl = ahwal_setting('whatshub.worker_url');
        $this->devID = ahwal_setting('whatshub.device_id');
    }

    public function render()
    {
        return view('whatshub::livewire.device-control', [
            'connection_update' => Cache::get("connection_update.{$this->devID}"),
        ]);
    }

    public function start()
    {
        Http::post($this->workerUrl . '/start', [
            'devID' => $this->devID,
            'webhook' => route('whatshub.wa-webhook'),
        ]);

        try {
        } catch (\Throwable $th) {
            $this->notify('Gagal!');
        }
    }

    public function getStatus()
    {
        $response = Http::post($this->workerUrl . '/status', ['devID' => $this->devID]);
        return $response->json();
    }

    public function stop()
    {
        Http::post($this->workerUrl . '/stop', ['devID' => $this->devID]);
    }

    public function doReset()
    {
        Http::post($this->workerUrl . '/reset', ['devID' => $this->devID]);

        $updatesFile = public_path('js/wa-updates.json');
        if (file_exists($updatesFile)) unlink($updatesFile);
    }

    public function sendMessage($phone = '', $message = '')
    {
        $response = Http::post($this->workerUrl . '/sendMessage', [
            'devID' => $this->devID,
            'number' => '6281227785005',
            'message' => [ 'text' => 'tes ' . date('Y-m-d H:i:s') ]
        ]);

        return $response->json();
    }
}
