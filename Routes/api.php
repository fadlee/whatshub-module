<?php

use Illuminate\Support\Facades\Route;
use Modules\Whatshub\Http\Controllers\WAWebhookController;

Route::prefix('whatshub')
    ->group(function () {
        Route::get('/wa-webhook', fn () => 'POST only');
        Route::post('/wa-webhook', WAWebhookController::class)
            ->name('whatshub.wa-webhook')
            ->middleware('throttle:120,1');
    });
