<?php
namespace Modules\Whatshub\Actions;

use Illuminate\Support\Str;
use Modules\Whatshub\Models\Outbox;

class PrepareOutboxReadyToSend
{
    public static function run()
    {
        /** @var Outbox $outbox */
        $outbox = Outbox::isReadyToSend()->first();

        if (!$outbox) {
            return null;
        }

        if ($outbox->number != 'status@broadcast') {
            // check if failed before
            $invalidCount = 0;
            do {
                $invalidCount = Outbox::where('number', $outbox->number)->where('status', -1)->count();
                if ($invalidCount >= 2) {
                    MarkInvalidNumberOutbox::run($outbox);
                    $outbox = Outbox::isReadyToSend()->first();
                }
            } while ($outbox && $invalidCount >= 2);
        }

        // send the message
        $message = [];

        if ($outbox->type == 'text') {
            $message['text'] = $outbox->message['text'];
        }

        // prepare image message
        if ($outbox->type == 'image') {
            if (!empty($outbox->message['image'])) {
                if (isset($message['text'])) {
                    unset($message['text']);
                }

                $message['image'] = $outbox->message['image'];

                if (!empty($outbox->message['caption'])) {
                    $message['caption'] = $outbox->message['caption'];
                }
            }
        }

        if ($outbox->number == 'status@broadcast') {
            $number = 'status@broadcast';
        } else {
            $number = preg_replace('/[^0-9]/', '', $outbox->number);

            // check for invalid number
            if (empty($number) || Str::length("$number") < 10) {
                MarkInvalidNumberOutbox::run($outbox);
                return self::run();
            }
        }

        return ['id' => $outbox->id, 'number' => $number, 'message' => $message, 'type' => $outbox->type];
    }
}
