<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('whatshub_broadcasts', function (Blueprint $table) {
            $table->id();

            $table->string('name');

            $table->json('lists');

            $table->text('messages');
            $table->string('status', 16);
            $table->json('meta');

            $table->timestamp('scheduled_at')->nullable();
            $table->timestamp('sent_at')->nullable();

            $table->timestamps();
        });

        Schema::table('whatshub_outbox', function (Blueprint $table) {
            $table->foreignId('broadcast_id')->nullable()->constrained('whatshub_broadcasts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('whatshub_outbox', function (Blueprint $table) {
            $table->dropForeign(['broadcast_id']);
            $table->dropColumn('broadcast_id');
        });

        Schema::dropIfExists('whatshub_broadcasts');
    }
};
