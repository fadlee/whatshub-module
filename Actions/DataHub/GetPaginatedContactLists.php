<?php

namespace Modules\Whatshub\Actions\DataHub;

use Modules\Whatshub\Models\ContactList;

class GetPaginatedContactLists
{
    public static function run($params = [])
    {
        $page = $params['page'] ?? 1;

        $query = ContactList::query();

        if (isset($params['filter'])) {
            $query->filter($params['filter']);
        }

        if (isset($params['relations']) && !empty($params['relations'])) {
            $query->with(explode(',', $params['relations']));
        }

        return $query->orderByDesc('id')
            ->paginate(30, ['*'], 'page', $page)->toArray();
    }
}
