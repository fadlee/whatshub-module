<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('whatshub_outbox', function (Blueprint $table) {
            $table->id();
            $table->string('number');
            $table->string('type', 16)->default('text');
            $table->tinyInteger('status')->default(0); // message_ack update (1 to 3), -1 means invalid number
            $table->string('key', 64)->nullable()->index();
            $table->json('message');

            $table->unsignedTinyInteger('priority')->default(10);

            $table->timestamp('scheduled_at')->nullable();
            $table->timestamp('sent_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('whatshub_outbox');
    }
};
