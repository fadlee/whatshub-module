<?php

namespace Modules\Whatshub\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Modules\Whatshub\Actions\MarkInvalidNumberOutbox;
use Modules\Whatshub\Actions\PrepareOutboxReadyToSend;
use Modules\Whatshub\Actions\SendRealtimeMessage;
use Modules\Whatshub\Models\Outbox;

class WAWebhookController extends Controller
{
    protected $request;
    protected $updates;

    public function __invoke(Request $request)
    {
        $action = request('action', 'default');

        Log::info($action);

        $data = 'invalid action';

        if (method_exists($this, $action)) {
            $data = $this->{$action}();
        } else {
            Log::info('Webhook action not exists: ' . $action);
        }

        return ['success' => true, 'data' => $data];
    }

    private function qr()
    {
        Cache::set('qr.' . request('data.devID'), request('data.qr'), 20);

        SendRealtimeMessage::make()
            ->event('qr')
            ->send(request('data.qr'));
    }

    private function connection_update()
    {
        $update = strtolower(request('data.update'));

        Log::info('connection_update: ' . request('data.devID') . ' - ' . $update);

        Cache::set('connection_update.' . request('data.devID'), $update);

        SendRealtimeMessage::make()
            ->event('connection_update')
            ->send($update);
    }

    private function ready_to_send()
    {
        $status = Cache::get('connection_update.' . request('data.devID'));

        if (!in_array($status, ['ready', 'connected'])) {
            // return ['connection_update' => $status];
        }

        $response = PrepareOutboxReadyToSend::run() ?? [];

        $response['connection_update'] = $status;

        return $response;
    }

    private function get_connection_update()
    {
        return Cache::get('connection_update.' . request('data.devID'));
    }

    private function invalid_number()
    {
        // TODO: mark invalid number to exclude from sending
        $number = request('data.number');
        $outbox = Outbox::findOrFail(request('data.outbox_id'));

        MarkInvalidNumberOutbox::run($outbox);

        SendRealtimeMessage::make()
            ->event('message_status')
            ->send(json_encode([
                'id' => $outbox->id,
                'status' => $outbox->status
            ]));
    }

    private function message_sent()
    {
        $outbox = Outbox::findOrFail(request('data.outbox_id'));

        $outbox->sent_at = Date::now();
        $outbox->key = request('data.key');

        $outbox->save();
    }

    private function message_ack()
    {
        $outbox = Outbox::where('key', request('data.key'))
            ->firstOrFail(['id', 'key']);

        $outbox->status = request('data.ack');
        $outbox->save();

        SendRealtimeMessage::make()
            ->event('message_status')
            ->send(json_encode([
                'id' => $outbox->id,
                'status' => $outbox->status
            ]));

        Log::debug($outbox);
    }

    private function debug_webhook()
    {
        Http::post('https://webhook.site/' . env('WEBHOOK_ID'), request('data'));
    }
}
