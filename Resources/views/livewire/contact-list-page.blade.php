@section('page-title', 'Contact List')

<div x-data="listPage">

    <x-ui::layout.list-with-detail title="List" selected-var="selectedData">
        <x-slot name="afterTitle">
            <x-ui::button.secondary
                x-on:click="newData"
                x-show="!selectedData"
                class="!ml-auto sm:!ml-4"
            >Baru</x-ui::button.secondary>
        </x-slot>

        <x-slot name="detailTitle">
            <span x-text="selectedData == 'new' ? 'List Baru' : 'Detil List'"></span>
        </x-slot>

        <x-slot name="list">
            <x-ui::select-list.generic
                id="select-list"
                data-hub="Whatshub::GetPaginatedContactLists"
                x-model="selectedData"
                x-on:change="bulkInput = false; input = $event.detail ? prepareForEdit($event.detail) : defaultInput"
                height="calc(100vh - 140px)"
                data-class="p-2 cursor-pointer group text-[13px]"
            >
                <div class="flex justify-between w-full">
                    <div x-text="item.data.number"></div>
                    <div class="text-gray-400" x-text="item.data.scheduled_at"></div>
                </div>
                <div class="flex mt-1">
                    <span x-text="item.data.name"></span>
                </div>
            </x-ui::select-list.generic>
        </x-slot>

        <x-slot name="detail">
            <div class="px-4 bg-white" x-ref="detail">
                    <form class="space-y-6 sm:space-y-5" x-on:submit.prevent="storeData">
                        <button class="js-submit" type="submit" x-ref="submit" hidden></button>

                        <x-ui::field-group label="Nama">
                            <x-ui::field-group.input
                                class="w-full max-w-lg sm:max-w-xs"
                                x-model="input.name"
                                x-ref="firstInput"
                                error="getError('input.name')"
                                required
                            />
                        </x-ui::field-group>

                        <x-ui::field-group label="Data Kontak">
                            <x-ui::field-group.textarea
                                class="w-full h-20 max-w-lg"
                                x-model="input.data"
                                error="getError('input.data')"
                            />
                            <x-ui::button.secondary
                                x-on:click="dataPreview = dataPreview ? null : window.parseTSV(input.data)"
                                x-show="input.data"
                                class="block mt-2"
                            >Preview</x-ui::button.secondary>
                            <pre x-json="dataPreview" x-show="dataPreview"></pre>
                        </x-ui::field-group>


                    </form>
            </div>
        </x-slot> <!-- name=detail -->

        <x-slot name="detailActions">
            <x-ui::button.primary x-on:click="$refs.detail.querySelector('.js-submit').click()">Simpan</x-ui::button.primary>

            <x-ui::button.secondary
                x-show="input.id"
                x-on:click="openConfirm('Mau menghapus data: #' + input.id + '?').then(async () => {
                    await $wire.delete(input.id)
                    xData('#select-list').refreshData()
                })"
                class="!border-red-500 !text-red-500 hover:bg-red-100 ml-auto"
            >Hapus</x-ui::button.secondary>
        </x-slot>
    </x-ui::layout.list-with-detail>

</div>

@push('scripts')
<script>
    function listPage() {
        return {
            selectedData: null,
            defaultInput: { name: '', data: '' },
            dataPreview: null,
            input: {},
            errors: {},

            async init() {
                this.input = _.cloneDeep(this.defaultInput)
            },

            newData() {
                this.selectedData = 'new'
                this.bulkInput = false
                this.input = _.cloneDeep(this.defaultInput)

                setTimeout(() => this.$nextTick(() => this.$refs.firstInput.focus()), 100)
            },
            newDataBulk() {
                this.inputBulk = { messages: [{type: 'text', text: ''}] }
                this.selectedData = 'new'
                this.bulkInput = true
            },

            prepareForEdit(input) {
                if (typeof input != 'object') return input

                errors = {}

                // {{-- delete uneditable attributes --}}
                delete input.created_at
                delete input.updated_at

                // prepare for datetime-local input
                if (input.scheduled_at) input.scheduled_at = input.scheduled_at.replace(' ', 'T')
                if (input.sent_at) input.sent_at = input.sent_at.replace(' ', 'T')

                return input
            },
            storeData() {
                this.$wire.store(this.input).then((resp) => {
                    if (!resp || resp.failed > 0) {
                        this.errors = livewireErrors('{{ $this->id }}')
                        return;
                    }
                    this.errors = {}
                    xData('#select-list').refreshData(resp)
                })
            },
            storeBulk() {
                const data = {
                    tsv: parseTSV(this.inputBulk.tsv),
                    messages: this.inputBulk.messages,
                    scheduled_at: this.inputBulk.scheduled_at,
                }
                console.log(data)
                this.$wire.storeBulk(data).then(resp => {
                    xData('#select-list').refreshData()
                })
            },
            getError(fieldName) {
                if (this.errors[fieldName] && Array.isArray(this.errors[fieldName])) {
                    return this.errors[fieldName][0]
                }

                return null
            },
        }
    }
</script>
@endpush
