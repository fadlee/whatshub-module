<?php

namespace Modules\Whatshub\Http\Livewire;

use Livewire\Component;
use Modules\Whatshub\Models\ContactList;

class ContactListPage extends Component
{
    public function render()
    {
        return view('whatshub::livewire.contact-list-page');
    }

    public function store($data)
    {
        if (isset($data['id'])) {
            return $this->update($data);
        }

        $outbox = ContactList::create($data);

        return $outbox->toArray();
    }

    private function update($data)
    {
        return ContactList::find($data['id'])->update($data);
    }

    public function delete(ContactList $data)
    {
        return $data->delete();
    }
}
