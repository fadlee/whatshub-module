@section('page-title', 'Outbox')

<div x-data="outboxPage">

    <x-ui::layout.list-with-detail title="Outbox" selected-var="selectedData">
        <x-slot name="afterTitle">
            <x-ui::button.secondary
                x-on:click="newData"
                x-show="!selectedData"
                class="!ml-auto sm:!ml-4"
            >Baru</x-ui::button.secondary>
            <x-ui::button.secondary
                x-on:click="newDataBulk"
                x-show="!selectedData"
                class="!ml-auto sm:!ml-4"
            >Input Massal</x-ui::button.secondary>
        </x-slot>

        <x-slot name="detailTitle">
            <span x-text="selectedData == 'new' ? 'Outbox Baru' : 'Detil Outbox'"></span>
        </x-slot>

        <x-slot name="list">
            <x-ui::select-list.generic
                id="select-list"
                data-hub="Whatshub::GetPaginatedOutbox"
                x-model="selectedData"
                x-on:change="bulkInput = false; input = $event.detail ? prepareForEdit($event.detail) : defaultInput"
                height="calc(100vh - 140px)"
                data-class="p-2 cursor-pointer group text-[13px]"
            >
                <div class="flex justify-between w-full">
                    <div x-text="item.data.number"></div>
                    <div class="text-gray-400" x-text="item.data.scheduled_at"></div>
                </div>
                <div class="flex mt-1">
                    <x-ui::svg x-show="item.data.status > 1" icon="dblcheck" class="flex-shrink-0 inline-block w-4 h-4"
                        x-bind:class="{'text-blue-500': item.data.status == 3}" />
                    <x-ui::svg x-show="item.data.status == 1" icon="check" class="flex-shrink-0 inline-block w-4 h-4" />
                    <x-ui::svg x-show="item.data.status == 0" icon="clock" class="flex-shrink-0 inline-block w-4 h-4" />
                    <x-ui::svg x-show="item.data.status < 0" icon="ban" class="flex-shrink-0 inline-block w-4 h-4 text-red-600" />

                    <div class="flex ml-1 truncate">
                        <span class="truncate" x-show="item.data.type == 'text'" x-text="item.data.message.text"></span>

                        <x-ui::svg x-show="item.data.type == 'image'" icon="image" class="inline-block w-5 h-5 mr-1" />
                        <span x-show="item.data.type == 'image' && item.data.message.caption == ''" x-text="'Photo'"></span>
                        <span class="truncate" x-show="item.data.type == 'image' && item.data.message.caption != ''" x-text="item.data.message.caption"></span>
                    </div>
                </div>
            </x-ui::select-list.generic>
        </x-slot>

        <x-slot name="detail">
            <div class="px-4 bg-white" x-ref="detail">

                <template x-if="!bulkInput">
                    <form class="space-y-6 sm:space-y-5" x-on:submit.prevent="storeData">
                        <button class="js-submit" type="submit" x-ref="submit" hidden></button>

                        <x-ui::field-group label="Nomor WA">
                            <x-ui::field-group.input
                                class="w-full max-w-lg sm:max-w-xs"
                                x-model="input.number"
                                x-ref="firstInput"
                                error="getError('input.number')"
                                required
                            />
                            <p class="mt-1 text-xs text-gray-500">Hanya angka. Format lengkap dengan awalan kode negara (62). <br>
                            Contoh: 6281234567890</p>
                        </x-ui::field-group>

                        <x-ui::field-group label="Jenis Pesan">
                            <x-ui::field-group.radio
                                :options="[
                                    'text' => 'Teks',
                                    'image' => 'Gambar',
                                ]"
                                x-model="input.type"
                            />
                        </x-ui::field-group>

                        <x-ui::field-group label="Isi Pesan" x-show="input.type == 'text'">
                            <x-ui::field-group.textarea
                                class="w-full h-20 max-w-lg"
                                x-model="input.message.text"
                                error="getError('input.message.text')"
                            />
                        </x-ui::field-group>

                        <x-ui::field-group label="URL Gambar" x-show="input.type == 'image'">
                            <x-ui::field-group.input
                                class="w-full max-w-lg"
                                x-model="input.message.image"
                                error="getError('input.message.image')"
                            />
                        </x-ui::field-group>

                        <x-ui::field-group label="Caption Gambar" x-show="input.type == 'image'">
                            <x-ui::field-group.textarea
                                class="w-full h-20 max-w-lg"
                                x-model="input.message.caption"
                                error="getError('input.message.caption')"
                            />
                        </x-ui::field-group>

                        {{-- <x-ui::field-group label="Prioritas">
                            <x-ui::field-group.input
                                type="number"
                                class="w-full max-w-lg sm:max-w-xs"
                                x-model="input.priority"
                                error="getError('input.priority')"
                            />
                        </x-ui::field-group> --}}

                        <x-ui::field-group label="Jadwalkan Pada">
                            <x-ui::field-group.input
                                type="datetime-local"
                                step="1"
                                class="w-full max-w-lg sm:max-w-xs"
                                x-model="input.scheduled_at"
                                error="getError('input.scheduled_at')"
                            />
                        </x-ui::field-group>

                        <x-ui::field-group label="Waktu Terkirim" x-show="input.sent_at">
                            <x-ui::field-group.input
                                type="datetime-local"
                                step="1"
                                class="w-full max-w-lg sm:max-w-xs"
                                x-model="input.sent_at"
                                error="getError('input.sent_at')"
                            />
                            <a x-show="input.sent_at" href="#" x-on:click.prevent="input.sent_at = null; input.status = 0">
                                <x-ui::svg icon="x" class="inline-block w-4 h-4 text-red-600" />
                            </a>
                            <p class="mt-1 text-xs text-gray-500">Pesan akan dikirim ulang jika dikosongkan.</p>
                        </x-ui::field-group>

                        <pre x-json="input"></pre>

                    </form>
                </template>

                <template x-if="bulkInput">
                    <form class="space-y-6 sm:space-y-5" x-on:submit.prevent="storeBulk">
                        <button class="js-submit" type="submit" x-ref="submit" hidden></button>

                        <h2 class="text-lg">Input Massal</h2>

                        <x-ui::field-group label="Nomor">
                            <x-ui::field-group.textarea
                                class="w-full h-20 font-mono"
                                x-model="inputBulk.tsv"
                                error="getError('inputBulk.tsv')"
                            />
                            <p class="mt-1 text-xs text-gray-500">
                                Copy-paste data dari Excel.
                                <br>Pastikan kolom pertama adalah nomor WA.
                                <br>Baris pertama adalah judul kolom.
                                <br>Contoh:
<pre class="p-1 text-xs text-gray-500 bg-gray-100">
nomor	nama
6281234567890	Ahmad
6281234567890	Budi
</pre>
                            </p>
                        </x-ui::field-group>

                        <x-ui::field-group label="Pesan">
                            <div class="divide-y divide-solid">
                                <div>
                                    <p class="block text-xs text-gray-500">
                                        Gunakan <code class="bg-gray-100">@{{kolom}}</code> untuk
                                        menyisipkan data yang dimasukkan di atas.<br>
                                        Contoh:
                                    </p>
                                    <pre class="p-1 text-xs text-gray-500 bg-gray-100"
                                        >Hai @{{nama}}, ini nomor Anda: @{{nomor}}</pre>
                                </div>

                                <template x-for="(msg, index) in inputBulk.messages" :key="index + '23523'" hidden>
                                    <div class="py-4">
                                        <div class="flex mb-2 space-x-2">
                                            <p x-text="'Pesan ' + parseInt(index + 1)"></p>
                                            <a href="#" class="text-red-500" x-on:click.prevent="inputBulk.messages.splice(index, 1)">x</a>
                                            <x-ui::button.secondary
                                                class="!ml-auto leading-4"
                                                x-bind:class="{'bg-gray-300': msg.type=='text'}"
                                                x-on:click="msg.type = 'text'"
                                                >Teks</x-ui::button.secondary>
                                            <x-ui::button.secondary
                                                class="leading-4"
                                                x-bind:class="{'bg-gray-300': msg.type=='image'}"
                                                x-on:click="msg.type = 'image'"
                                                >Gambar</x-ui::button.secondary>
                                        </div>

                                        <x-ui::field-group.textarea
                                            x-show="msg.type == 'text'"
                                            class="w-full h-20 max-w-lg"
                                            x-model="msg.text"
                                            error="false"
                                        ></x-ui::field-group.textarea>

                                        <x-ui::field-group.input
                                            x-show="msg.type == 'image'"
                                            class="w-full max-w-lg"
                                            x-model="msg.image"
                                            error="false"
                                            placeholder="URL Gambar"
                                        />
                                        <x-ui::field-group.textarea
                                            x-show="msg.type == 'image'"
                                            class="w-full h-20 max-w-lg mt-1"
                                            x-model="msg.caption"
                                            error="false"
                                            placeholder="Caption"
                                        ></x-ui::field-group.textarea>

                                        {{-- <pre x-json="msg"></pre> --}}
                                    </div>
                                </template>

                                <x-ui::button.secondary x-on:click="inputBulk.messages.push({type: 'text'})" class="mt-4">Tambah Pesan</x-ui::button.secondary>
                            </div>
                        </x-ui::field-group>

                        <x-ui::field-group label="Jadwalkan Pada">
                            <x-ui::field-group.input
                                type="datetime-local"
                                step="1"
                                class="w-full max-w-lg sm:max-w-xs"
                                x-model="inputBulk.scheduled_at"
                                error="getError('inputBulk.scheduled_at')"
                            />
                        </x-ui::field-group>


                        {{-- <pre x-json="input"></pre> --}}
                    </form>
                </template>
            </div>
        </x-slot> <!-- name=detail -->

        <x-slot name="detailActions">
            <x-ui::button.primary x-on:click="$refs.detail.querySelector('.js-submit').click()">Simpan</x-ui::button.primary>

            <x-ui::button.secondary
                x-show="input.id"
                x-on:click="openConfirm('Mau menghapus data: #' + input.id + '?').then(async () => {
                    await $wire.delete(input.id)
                    xData('#select-list').refreshData()
                })"
                class="!border-red-500 !text-red-500 hover:bg-red-100 ml-auto"
            >Hapus</x-ui::button.secondary>
        </x-slot>
    </x-ui::layout.list-with-detail>

</div>

@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/ably@1.2.14/browser/static/ably-commonjs.min.js"></script>
<script>window.ABLY_KEY = '{{ ahwal_setting('whatshub.ably_subscribe_key') }}'</script>

<script>
    function outboxPage() {
        return {
            selectedData: null,
            defaultInput: { priority: 10, type: 'text', message: {} },
            input: {},
            errors: {},
            bulkInput: false,
            inputBulk: { messages: [{type: 'text', text: ''}] },

            async init() {
                this.input = _.cloneDeep(this.defaultInput)

                {{-- this.$watch('input.type', val => {
                    if (val == 'text') {
                        this.input.message = { text: '' }
                    }
                    if (val == 'image') {
                        this.input.message = { caption: '', image: '' }
                    }
                }) --}}

                var ably = new Ably.Realtime(window.ABLY_KEY)
                var channel = ably.channels.get('main')

                channel.subscribe('message_status', msg => {
                    console.log(msg)
                    const message = JSON.parse(msg.data)
                    xData('#select-list').updateData(message.id, {status: message.status})
                })
            },

            newData() {
                this.selectedData = 'new'
                this.bulkInput = false
                this.input = _.cloneDeep(this.defaultInput)

                setTimeout(() => this.$nextTick(() => this.$refs.firstInput.focus()), 100)
            },
            newDataBulk() {
                this.inputBulk = { messages: [{type: 'text', text: ''}] }
                this.selectedData = 'new'
                this.bulkInput = true
            },

            prepareForEdit(input) {
                if (typeof input != 'object') return input

                errors = {}

                // {{-- delete uneditable attributes --}}
                delete input.created_at
                delete input.updated_at

                // prepare for datetime-local input
                if (input.scheduled_at) input.scheduled_at = input.scheduled_at.replace(' ', 'T')
                if (input.sent_at) input.sent_at = input.sent_at.replace(' ', 'T')

                return input
            },
            storeData() {
                this.$wire.store(this.input).then((resp) => {
                    if (!resp || resp.failed > 0) {
                        this.errors = livewireErrors('{{ $this->id }}')
                        return;
                    }
                    this.errors = {}
                    xData('#select-list').refreshData(resp)
                })
            },
            storeBulk() {
                const data = {
                    tsv: parseTSV(this.inputBulk.tsv),
                    messages: this.inputBulk.messages,
                    scheduled_at: this.inputBulk.scheduled_at,
                }
                console.log(data)
                this.$wire.storeBulk(data).then(resp => {
                    xData('#select-list').refreshData()
                })
            },
            getError(fieldName) {
                if (this.errors[fieldName] && Array.isArray(this.errors[fieldName])) {
                    return this.errors[fieldName][0]
                }

                return null
            },
        }
    }
</script>
@endpush
