<?php

namespace Modules\Whatshub\Http\Livewire;

use Livewire\Component;

class WhatshubPage extends Component
{
    public function render()
    {
        return view('whatshub::livewire.whatshub-page');
    }
}
