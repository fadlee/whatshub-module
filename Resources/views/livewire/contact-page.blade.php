@section('page-title', 'Kontak - Whatshub')

<div x-data="{
    selectedPermission: null,

    prepareForEdit(input) {
        if (typeof input != 'object') return input

        errors = {}

        {{-- delete uneditable attributes --}}
        delete input.created_at
        delete input.updated_at

        return input
    },


    input: {},
    store() {
        this.$wire.store(this.input).then((resp) => {
            if (!resp || resp.failed > 0) {
                this.errors = livewireErrors('{{ $this->id }}')
                return;
            }
            this.errors = {}
            xData('#permission-select-list').refreshData(resp)
        })
    },
    confirmAndDelete() {
        openConfirm('Mau menghapus data ini?').then(async () => {
            await this.$wire.delete(this.input.id)
            xData('#permission-select-list').refreshData()
        })
    },


    errors: {},
    getError(fieldName) {
        if (this.errors[fieldName] && Array.isArray(this.errors[fieldName])) {
            return this.errors[fieldName][0]
        }
        return null
    },

}" class="h-screen pt-10 bg-white">

    <div class="flex p-4 space-x-2">
        <h1 class="text-lg font-medium leading-6 text-gray-900">Kampanye</h1>
        <x-ui::button.secondary x-on:click="selectedPermission = 'new'; input = {}; $nextTick(() => $refs.firstInput.focus())">Baru</x-ui::button.secondary>
    </div>

    <div class="md:grid md:grid-cols-[300px,1fr] gap-4 md:h-[calc(100vh-140px)] px-4">

        <x-ui::select-list.generic
            id="permission-select-list"
            data-hub="getPermissions"
            x-model="selectedPermission"
            x-on:change="input = $event.detail ? prepareForEdit($event.detail) : {}"
            height="calc(100vh - 140px)"
        >
            <div x-text="item.data.name" class="px-1"></div>
        </x-ui::select-list.generic>

        <div class="mt-4 overflow-y-auto border shadow md:mt-0">
            <div x-show="!selectedPermission" class="grid h-full place-content-center">
                Belum ada data yang dipilih
            </div>
            <div x-show="selectedPermission">
                <form x-on:submit.prevent="store">
                    <div class="px-4 py-6 space-y-6 bg-white sm:p-6">
                        <div class="flex">
                            <h3 class="text-lg font-medium leading-6 text-gray-900">Detil Hak Akses</h3>
                            {{-- <p class="mt-1 text-sm text-gray-500" x-text="selectedPermission">Data User.</p> --}}
                            <x-ui::button.secondary
                                x-on:click="confirmAndDelete"
                                class="!border-red-500 !text-red-500 ml-auto">Hapus</x-ui::button.secondary>
                        </div>

                        <div class="space-y-6 sm:space-y-5">

                            <div class="text-sm sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
                                <span class="block font-medium text-gray-700 sm:mt-px sm:pt-1">
                                    Nama Hak Akses
                                </span>
                                <div class="mt-1 sm:mt-0 sm:col-span-2">
                                    <x-ui::input
                                        x-model="input.name" class="w-full max-w-lg sm:max-w-xs"
                                        x-bind:class="{
                                            '!border-red-500 focus:!ring-red-500': getError('input.name')
                                        }"
                                        x-ref="firstInput"
                                    />
                                    <p x-text="getError('input.name')" class="mt-1 text-xs text-red-500"></p>
                                </div>
                            </div>

                            {{-- <pre x-json="input"></pre> --}}

                        </div>

                    </div>
                    <div class="px-4 py-3 text-right border-t sm:px-6">
                        <x-ui::button.primary>Simpan</x-ui::button.primary>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
