<?php

namespace Modules\Whatshub\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\AsArrayObject;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Date;

class ContactList extends Model
{
    use HasFactory;

    protected $table = 'whatshub_contact_lists';

    protected $fillable = [
        'name',
        'data',
        'stats',
    ];

    protected $casts = [
        'stats' => AsArrayObject::class,
    ];

    /**
     * Get the campaign that owns the Outbox
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function campaign(): BelongsTo
    {
        return $this->belongsTo(Campaign::class);
    }

    /**
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsReadyToSend($query)
    {
        return $query
            ->where('status', '>=', 0)
            ->where('sent_at', '=', null)
            ->where('number', '!=', '')
            ->where(function(Builder $_query) {
                $_query->where('scheduled_at', '<=', Date::now()->format('Y-m-d H:i:s'))
                    ->orWhereNull('scheduled_at');
            })
            ->orderBy('scheduled_at');
    }
}
