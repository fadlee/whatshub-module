<div x-data="deviceControlComponent" class="mt-8 sm:mx-auto sm:w-full sm:max-w-sm">
    <div class="px-4 py-8 bg-white shadow sm:rounded-lg sm:px-10">
        <div class="space-y-6">
        <template x-if="typeof qrcode == 'string' && qrcode.length" hidden>
            <div x-qrcode="qrcode" class="w-[300px]"></div>
        </template>

        <pre x-json="status"></pre>
        <pre x-json="connectionUpdate"></pre>

        <div class="space-y-4">
            <button
                x-show="canStart()"
                type="submit" class="flex justify-center w-full px-4 py-2 text-sm font-medium text-white bg-green-600 border border-transparent rounded-md shadow-sm hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                x-on:click="$wire.start(); status.isActive = true"
                x-text="status.hasSession ? 'Mulai' : 'Hubungkan Perangkat'"
            ></button>

            <button
                x-show="canStop()"
                type="submit"
                class="flex justify-center w-full px-4 py-2 text-sm font-medium text-white bg-yellow-600 border border-transparent rounded-md shadow-sm hover:bg-yellow-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                x-on:click="$wire.stop()"
            >Hentikan</button>

        </div>
        </div>

        <div class="mt-6 space-y-2 text-sm text-center">
            <p><a x-on:click="doReset()"
                x-show="canReset()"
                href="#" class="font-medium text-indigo-600 hover:text-indigo-500">
                Putuskan Perangkat
            </a></p>
        </div>

    </div>
</div>

@push('scripts')

    <script src="https://cdn.jsdelivr.net/npm/ably@1.2.14/browser/static/ably-commonjs.min.js"></script>
    <script>window.ABLY_KEY = '{{ ahwal_setting('whatshub.ably_subscribe_key') }}'</script>


    <script>
    function deviceControlComponent() {
        return {
            qrcode: '',
            status: {hasSession: false, isActive: false},
            connectionUpdate: {!! $connection_update ? "'{$connection_update}'" : 'null' !!},
            async init() {
                const _this = this

                var ably = new Ably.Realtime(window.ABLY_KEY)
                var channel = ably.channels.get('main')

                channel.subscribe('qr', message => {
                    this.qrcode = message.data
                })

                channel.subscribe('connection_update', update => {
                    this.connectionUpdate = update.data

                    if (update.data == 'ready' || update.data == 'destroyed')
                        this.checkStatus()

                    if (update.data != 'authenticating')
                        this.qrcode = ''

                    console.log('connection_update', update.data)
                })

                this.checkStatus()
            },
            async checkStatus() {
                const resp = await this.$wire.getStatus()
                if (resp) {
                    this.status = resp
                }
            },
            canStart() {
                return !this.status.isActive
            },
            canStop() {
                return this.status.isActive
            },

            canReset() {
                return this.status.hasSession
            },
            doReset() {
                this.$wire.doReset()
            }
        }
    }
    </script>
@endpush
