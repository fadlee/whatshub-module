<?php

return [
    'name' => 'Whatshub',
    'menu' => [
        'admin' => [
            'whatshub' => [
                'position' => 3,
                'route' => 'admin.whatshub.index',
                'icon' => 'user',
                'label' => 'Whatshub',
                'children' => [
                    [
                        'route' => 'admin.whatshub.index',
                        'label' => 'Beranda',
                    ],
                    [
                        'route' => 'admin.whatshub.outbox',
                        'label' => 'Outbox',
                    ],
                    [
                        'route' => 'admin.whatshub.list',
                        'label' => 'List',
                    ],
                    [
                        'link' => '#',
                        'label' => 'Automation',
                        'children' => [
                            [
                                'link' => '#',
                                'label' => 'Auto Forward'
                            ],
                            [
                                'link' => '#',
                                'label' => 'Auto Reply'
                            ],
                        ]
                    ],
                ],
            ],
        ],
    ],

    'settings' => [
        'whatshub' => [
            'label' => 'Whatshub',
            'items' => [
                [
                    'name' => 'whatshub.worker_url',
                    'type' => 'text',
                    'label' => 'Worker URL',
                    'default' => 'http://localhost:3288'
                ],
                [
                    'name' => 'whatshub.device_id',
                    'type' => 'text',
                    'label' => 'Device ID'
                ],
                [
                    'name' => 'whatshub.ably_key',
                    'type' => 'text',
                    'label' => 'Ably Key',
                ],
                [
                    'name' => 'whatshub.ably_subscribe_key',
                    'type' => 'text',
                    'label' => 'Ably Key (subscribe)',
                ],
                [
                    'name' => 'whatshub.auto_restart',
                    'type' => 'toggle',
                    'label' => 'Auto Restart'
                ],
            ]
        ]
    ],

];
