<?php

namespace Modules\Whatshub\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\AsArrayObject;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Date;

class Outbox extends Model
{
    use HasFactory;

    protected $table = 'whatshub_outbox';

    protected $fillable = [
        'number',
        'message',
        'type',
        'priority',
        'scheduled_at',
        'sent_at',
        'status',
    ];

    protected $casts = [
        'sent_at' => 'datetime:Y-m-d H:i:s',
        'scheduled_at' => 'datetime:Y-m-d H:i:s',
        'message' => AsArrayObject::class,
    ];

    protected static function booted()
    {
        static::creating(function($item) {
            if (empty($item->scheduled_at))
                $item->scheduled_at = Date::now();
        });
    }

    /**
     * Get the campaign that owns the Outbox
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function campaign(): BelongsTo
    {
        return $this->belongsTo(Campaign::class);
    }

    /**
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsReadyToSend($query)
    {
        return $query
            ->where('status', '>=', 0)
            ->where('sent_at', '=', null)
            ->where('number', '!=', '')
            ->where(function(Builder $_query) {
                $_query->where('scheduled_at', '<=', Date::now()->format('Y-m-d H:i:s'))
                    ->orWhereNull('scheduled_at');
            })
            ->orderBy('scheduled_at');
    }
}
