<?php

namespace Modules\Whatshub\Http\Livewire;

use Illuminate\Support\Facades\Date;
use Livewire\Component;
use Modules\Whatshub\Models\Outbox;

class OutboxPage extends Component
{
    public function render()
    {
        return view('whatshub::livewire.outbox-page');
    }

    public function store($data)
    {
        if (isset($data['id'])) {
            return $this->update($data);
        }

        $outbox = Outbox::create($data);

        return $outbox->toArray();
    }

    private function update($data)
    {
        return Outbox::find($data['id'])->update($data);
    }

    public function delete(Outbox $data)
    {
        return $data->delete();
    }

    public function storeBulk($data)
    {
        $tsv_headers = array_shift($data['tsv']);

        $insert = [];

        $schedule = isset($data['scheduled_at']) ? Date::parse($data['scheduled_at']) : Date::now();

        foreach ($data['tsv'] as $row) {
            foreach($data['messages'] as $message) {
                $message_text = $message['text'] ?? '';
                $message_caption = $message['caption'] ?? '';

                foreach ($tsv_headers as $idx => $key) {
                    $message_text = str_replace('{{' . $key . '}}', $row[$idx], $message_text);
                    $message_caption = str_replace('{{' . $key . '}}', $row[$idx], $message_caption);
                }

                $the_message = [];

                if ($message['type'] == 'text') {
                    $the_message['text'] = $message_text;
                }

                if ($message['type'] == 'image') {
                    $the_message['image'] = $message['image'];
                    $the_message['caption'] = $message_caption;
                }

                $insert[] = [
                    'number' => $row[0],
                    'type' => $message['type'],
                    'message' => json_encode($the_message),
                    'scheduled_at' => $schedule->addSecond()->format('Y-m-d H:i:s'),
                ];
            }
        }

        Outbox::insert($insert);
    }
}
