<?php

namespace Modules\Whatshub\Models;

use Illuminate\Database\Eloquent\Casts\AsArrayObject;
use Illuminate\Database\Eloquent\Casts\AsCollection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Campaign extends Model
{
    use HasFactory;

    protected $table = 'whatshub_campaigns';

    protected $fillable = ['name', 'contacts', 'message', 'progress'];

    protected $casts = [
        'contacts' => AsCollection::class,
        'message' => AsArrayObject::class,
        'progress' => AsArrayObject::class,
    ];

    /**
     * Get all of the outbox for the Campaign
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function outbox(): HasMany
    {
        return $this->hasMany(Outbox::class);
    }
}
