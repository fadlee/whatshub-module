<?php

namespace Modules\Whatshub\Http\Livewire;

use Livewire\Component;

class CampaignPage extends Component
{
    public function render()
    {
        return view('whatshub::livewire.campaign-page');
    }
}
